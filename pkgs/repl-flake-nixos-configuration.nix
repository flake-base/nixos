{ writeText
, writeShellScript
, inetutils
}:

let

  name = "repl-flake-nixos-configuration";

  nixFile = writeText "${name}.nix" ''
    { flakePath, nixosConfigurationName }:
    let
      flake = builtins.getFlake (builtins.toString flakePath);
    in
    flake.nixosConfigurations.''${nixosConfigurationName} // { inherit flake; }
  '';

in
writeShellScript "${name}.sh" ''

  if [ "$1" = "--help" ]; then
    echo "Usage: $0 <flake-path> [nixos-configuration-name]" >&2
    exit 1
  fi

  flakePath="$1"
  shift

  nixosConfigurationName="$1"
  shift

  if ! [ -r "$flakePath" ]; then
    echo "The first argument must by a valid Flake path!" >&2
    exit 2
  fi

  if [ -z "$nixosConfigurationName" ]; then
    nixosConfigurationName=$(${inetutils}/bin/hostname -s)
    echo "Setting name of the nixosConfiguration to default hostname: $nixosConfigurationName" >&2
  fi

  echo
  echo "# Running nix repl with the nixosConfigurations.$nixosConfigurationName from $flakePath ..." >&2
  http_proxy= https_proxy= exec nix repl \
  --argstr flakePath "$flakePath" \
  --argstr nixosConfigurationName "$nixosConfigurationName" \
  ${nixFile} $@

''

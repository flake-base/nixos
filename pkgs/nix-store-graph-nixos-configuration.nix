{ writeShellScript
, graphviz
, xdot
}:

let

  name = "nix-store-graph-nixos-configuration";

in
writeShellScript "${name}.sh" ''

  if [ "$1" = "--help" ]; then
    echo "Usage: $0" >&2
    exit 1
  fi

  outLink="./result"

  if ! [ -L "$outLink" ]; then
    echo "No $outLink symlink!" >&2
    echo "Have you build nixosConfiguration before?" >&2
    exit 2
  fi

  echo
  echo "# Viewing the dependency graph for $outLink (click a node or halfs of an edge to navigate) ..." >&2
  nix-store --query --graph "$outLink" $@ | ${graphviz}/bin/dot -Txdot | exec ${xdot}/bin/xdot -n -

''

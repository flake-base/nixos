{ lib
, writeShellScript
, gnupg
, gnugrep
, defaultSopsCfg ? "./.sops.yaml"
}:

let

  name = "sops-config-update-keys";

in
writeShellScript "${name}.sh" ''

  if [ "$1" = "--help" -o ! -r "${defaultSopsCfg}" ]; then
    echo "Usage: $0" >&2
    echo "Create the sops' config file from armored key files in ./sops-keys/{hosts,users} directories." >&2
    echo "By default, the sops's config file ${defaultSopsCfg} will be created." >&2
    exit 1
  fi

  # see https://github.com/mozilla/sops#using-sops-yaml-conf-to-select-kms-pgp-for-new-files

  unset SOPS_PGP_FP
  for I in ./sops-keys/hosts/*.asc ./sops-keys/users/*.asc; do
    [ -r "$I" ] || continue  # continue is the file is not readable
    PGP_FP=`${gnupg}/bin/gpg --with-fingerprint --with-colons --import-options show-only --import "$I" | ${gnugrep}/bin/grep -Fm 1 'fpr:' | cut -d : -f 10`
    echo "$I: $PGP_FP" >&2
    SOPS_PGP_FP="$SOPS_PGP_FP,$PGP_FP"
    ${gnupg}/bin/gpg --verbose --import "$I"
  done

  exec cat >${defaultSopsCfg} <<END
  creation_rules:
  - pgp: ''${SOPS_PGP_FP#,}
  END

''

{ writeShellScript
}:

let

  name = "closure-size-nixos-configuration";

in
writeShellScript "${name}.sh" ''

  if [ "$1" = "--help" ]; then
    echo "Usage: $0" >&2
    exit 1
  fi

  outLink="./result"

  if ! [ -L "$outLink" ]; then
    echo "No $outLink symlink!" >&2
    echo "Have you build nixosConfiguration before?" >&2
    exit 2
  fi

  echo
  echo "# Getting the clouse-size of $outLink ..." >&2
  exec nix path-info --closure-size --human-readable "$outLink" $@

''

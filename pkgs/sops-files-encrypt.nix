{ lib
, writeShellScript
, findutils
, file
, gnugrep
, sops
, defaultSopsCfg ? "./.sops.yaml"
, defaultFiles ? "./*"
, defaultPlainExt ? ".plaintext"
}:

let

  name = "sops-files-encrypt";

  # `test F1 -nt F2` is true if F1 is newer (by the modification time set by `touch -m F1`) than F2 or if F2 does not exists
  findScript = ext: sopsConfig: ''
    I="{}"
    O="''${I%${ext}}"
    echo "# Checking file $O" >&2
    [ "$I" -nt "$O" -a -r "$I" -a ! -L "$I" ] \
    && [ "$(${file}/bin/file --brief --mime-type '$O')" = "application/json" -o ! -e "$O" ] \
    && if ${gnugrep}/bin/grep -qF '"data": "ENC' "$O" 2>/dev/null; then
      echo "# Encrypting (binary) file $I" >&2
      exec ${sops}/bin/sops --verbose --config "${sopsConfig}" --encrypt --input-type binary --output-type binary --output "$O" "$I"
    else
      echo "# Encrypting (auto) file $I" >&2
      exec ${sops}/bin/sops --verbose --config "${sopsConfig}" --encrypt --output "$O" "$I"
    fi
  '';

in
writeShellScript "${name}.sh" ''

  FILES="$(ls -d ''${1:-${defaultFiles}} 2>/dev/null)"

  if [ "$1" = "--help" -o -z "$FILES" ]; then
    echo "Usage: $0 [sops-files${defaultPlainExt}]" >&2
    echo "Use the sops' config file ${defaultSopsCfg} and encrypt the given files with the ${defaultPlainExt} extension by sops files without the extension." >&2
    echo "By default, the following ${defaultPlainExt} files will be encryted: ${defaultFiles}" >&2
    exit 1
  fi

  exec ${findutils}/bin/find $FILES -type f -name "*${defaultPlainExt}" -exec sh -c ${lib.strings.escapeShellArg (findScript defaultPlainExt defaultSopsCfg)} \;

''

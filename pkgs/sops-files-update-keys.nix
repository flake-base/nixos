{ lib
, writeShellScript
, findutils
, file
, gnugrep
, sops
, defaultSopsCfg ? "./.sops.yaml"
, defaultFiles ? "./*"
}:

let

  name = "sops-files-update-keys";

  # `test F1 -nt F2` is true if F1 is newer (by the modification time set by `touch -m F1`) than F2 or if F2 does not exists
  findScript = sopsConfig: ''
    I="{}"
    [ `${file}/bin/file --brief --mime-type "$I"` = "application/json" ] \
    && if ${gnugrep}/bin/grep -qF '"data": "ENC' "$I" 2>/dev/null; then
      echo "# Updating keys in (binary) file $I"
      # it seems that input/output types binary are ignored and *.yaml files that were internally JSONs are translated to YAML
      # WORKAROUND: do not use *.yaml file names
      exec ${sops}/bin/sops --verbose --config "${sopsConfig}" --input-type binary --output-type binary updatekeys --yes "$I"
    else
      echo "# Updating keys in (auto) file $I"
      exec ${sops}/bin/sops --verbose --config "${sopsConfig}" updatekeys --yes "$I"
    fi
  '';

in
writeShellScript "${name}.sh" ''

  FILES="$(ls -d ''${1:-${defaultFiles}} 2>/dev/null)"

  if [ "$1" = "--help" -o -z "$FILES" ]; then
    echo "Usage: $0 [sops-files]" >&2
    echo "Use the sops' config file ${defaultSopsCfg} and update keys in given sops files." >&2
    echo "By default, the following sops files will be updated: ${defaultFiles}" >&2
    exit 1
  fi

  exec ${findutils}/bin/find $FILES -type f -exec sh -c ${lib.strings.escapeShellArg (findScript defaultSopsCfg)} \;

''

{ writeShellScript
, inetutils
}:

let

  name = "nixos-rebuild-boot-flake-nixos-configuration";

in
writeShellScript "${name}.sh" ''

  if [ "$1" = "--help" ]; then
    echo "Usage: $0 <flake-path> [nixos-configuration-name]" >&2
    exit 1
  fi

  flakePath="$1"
  shift

  nixosConfigurationName="$1"
  shift

  if ! [ -r "$flakePath" ]; then
    echo "The first argument must by a valid Flake path!" >&2
    exit 2
  fi

  if [ -z "$nixosConfigurationName" ]; then
    nixosConfigurationName=$(${inetutils}/bin/hostname -s)
    echo "Setting name of the nixosConfiguration to default hostname: $nixosConfigurationName" >&2
  fi

  echo "# Building Virtual Machine for the nixosConfigurations.$nixosConfigurationName from $flakePath ..." >&2
  http_proxy= https_proxy= exec nixos-rebuild \
  --verbose --flake "$flakePath#$nixosConfigurationName" build-vm -L $@

''

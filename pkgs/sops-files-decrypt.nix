{ lib
, writeShellScript
, findutils
, file
, gnugrep
, sops
, defaultSopsCfg ? "./.sops.yaml"
, defaultFiles ? "./*"
, defaultPlainExt ? ".plaintext"
}:

let

  name = "sops-files-decrypt";

  # `test F1 -nt F2` is true if F1 is newer (by the modification time set by `touch -m F1`) than F2 or if F2 does not exists
  findScript = ext: sopsConfig: ''
    I="{}"
    O="$I${ext}"
    echo "# Checking file $O" >&2
    [ "$I" -nt "$O" -a -r "$I" -a ! -L "$I" ] \
    && [ "$(${file}/bin/file --brief --mime-type '$I')" = "application/json" ] \
    && ${gnugrep}/bin/grep -qF '"sops": {' "$I" \
    && echo "# Decrypting (binary) file $I" >&2 \
    && ${sops}/bin/sops --verbose --config "${sopsConfig}" --decrypt --input-type binary --output-type binary --output "$O" "$I" \
    && chmod g=,o= "$O" \
    && touch --no-create -m "--reference=$I" "$O"
  '';

in
writeShellScript "${name}.sh" ''

  FILES="$(ls -d ''${1:-${defaultFiles}} 2>/dev/null)"

  if [ "$1" = "--help" -o -z "$FILES" ]; then
    echo "Usage: $0 [sops-files]" >&2
    echo "Use the sops' config file ${defaultSopsCfg} and decrypt the given sops files to files with the ${defaultPlainExt} extension." >&2
    echo "By default, the following sops files will be decryted: ${defaultFiles}" >&2
    exit 1
  fi

  exec ${findutils}/bin/find $FILES -type f -exec sh -c ${lib.strings.escapeShellArg (findScript defaultPlainExt defaultSopsCfg)} \;

''

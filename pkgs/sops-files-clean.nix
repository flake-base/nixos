{ lib
, writeShellScript
, findutils
, defaultFiles ? "./*"
, defaultPlainExt ? ".plaintext"
}:

let

  name = "sops-files-clean";

  # `test F1 -nt F2` is true if F1 is newer (by the modification time set by `touch -m F1`) than F2 or if F2 does not exists
  findScript = ext: ''
    I="{}"
    O="''${I%${ext}}"
    if [ -r "$O" -a ! "$I" -nt "$O" ]; then
      echo "# Removing file $I"
      exec rm -v "$I"
    fi
  '';

in
writeShellScript "${name}.sh" ''

  FILES="$(ls -d ''${1:-${defaultFiles}} 2>/dev/null)"

  if [ "$1" = "--help" -o -z "$FILES" ]; then
    echo "Usage: $0 [sops-files${defaultPlainExt}]" >&2
    echo "Remove the given files with the ${defaultPlainExt} extension iff the corresponding sops files without the extension exists and are not older." >&2
    echo "By default, the following ${defaultPlainExt} files will be removed: ${defaultFiles}" >&2
    exit 1
  fi

  exec ${findutils}/bin/find $FILES -type f -name "*${defaultPlainExt}" -exec sh -c ${lib.strings.escapeShellArg (findScript defaultPlainExt)} \;

''

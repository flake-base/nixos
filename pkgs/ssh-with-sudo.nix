{ writeShellScript
, openssh
}:

let

  name = "ssh-with-sudo";

in
writeShellScript "${name}.sh" ''

  if [ "$1" = "--help" ]; then
    echo "Usage: $0 [command...]" >&2
    echo "With respect to the following envirinment variables: SSH_HOST SSH_USER SSH_IDENTITY SSH_OPTS SUDO_USER" >&2
    exit 1
  fi

  if [ -z "$SSH_HOST" ]; then
    echo "The SSH_HOST environment variable must by a valid hostname!" >&2
    exit 2
  fi

  if [ -n "$SSH_USER" ]; then
    SSH_OPTS="-l $SSH_USER $SSH_OPTS"
  fi

  if [ -r "$SSH_IDENTITY" ]; then
    SSH_OPTS="-i $SSH_IDENTITY $SSH_OPTS"
  fi

  if [ -n "$SUDO_USER" ]; then
    SUDO="sudo -Siu $SUDO_USER --"
  fi

  ${openssh}/bin/ssh -C $SSH_OPTS $SSH_HOST -- $SUDO $@

''

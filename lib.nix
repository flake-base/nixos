{ lib, pkgs, selfPkgs, ... }:

let

  stdenvMinimal = pkgs.stdenvNoCC.override {
    cc = null;
    preHook = "";
    allowedRequisites = null;
    initialPath = lib.filter
      (a: lib.hasPrefix "coreutils" a.name)
      pkgs.stdenvNoCC.initialPath;
    extraNativeBuildInputs = [ ];
  };

  # see https://fzakaria.com/2021/08/02/a-minimal-nix-shell.html
  mkMinimalShell = pkgs.mkShell.override {
    stdenv = stdenvMinimal;
  };

  mkPreCommitCheckForNix = pre-commit-hooks: src: nixLinterChecks: pre-commit-hooks.lib.${pkgs.system}.run {
    inherit src;
    hooks = {
      nixpkgs-fmt.enable = true;
      nix-linter =
        let
          nix-linter-with-checks = selfPkgs.nix-linter-with-checks.override {
            inherit (pre-commit-hooks.packages.${pkgs.system}) nix-linter;
            inherit nixLinterChecks;
          };
        in
        {
          enable = true;
          entry = lib.mkForce "${nix-linter-with-checks}/bin/nix-linter-with-checks";
        };
    };
  };

  mkSeqCheck = values:
    let
      results = builtins.map (value: builtins.seq value true) values;
      successful = builtins.all (result: result) results;
    in
    pkgs.runCommand "lib-check" { } ''
      touch $out
      exit ${if successful then "0" else "1"}
    '';

in
{

  inherit
    mkMinimalShell
    mkPreCommitCheckForNix
    mkSeqCheck
    stdenvMinimal
    ;

}

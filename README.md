# Nix Flake Library for NixOS Configuration and Deployment

## Usage

~~~nix
{

  description = "Sample NixOS configurations for some nodes.";

  inputs = {

    utils.url = "github:numtide/flake-utils";

    nixpkgs.url = "nixpkgs/release-21.11";

    nixpkgs-unstable.url = "nixpkgs/master";

    flake-base-nixos = {
      url = "gitlab:flake-base/nixos";
      inputs.utils.follows = "utils";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixpkgs-unstable.follows = "nixpkgs-unstable";
    };

    simple-nixos-mailserver = {
      # https://gitlab.com/simple-nixos-mailserver/nixos-mailserver
      url = "gitlab:simple-nixos-mailserver/nixos-mailserver/nixos-21.11";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.utils.follows = "utils";
    };

  };

  outputs = { self, flake-base-nixos, ... } @ args: flake-base-nixos.lib.mkOutputsWithAdditionalInputs args {
    nodesDirPath = self.outPath + "/nodes";
    loadedNixosModules.custom = {
      # set some NixOS config options
    };
    loadedHomeManagerModules.custom = {
      # set some HomeManager config options
    };
  };

}
~~~

## Node Configuration

For each **node name** in the nodes directory (`./nodes` by default), there must/should be its corresponding subdirectory with the following files/directories:

### MUST

*	`default.nix` with NixOS configuration of the node

### SHOULD

*	`system-arch.nix` with "system" and "crossSystem" configuration options corresponding to the node's architecture
*	`deploy-rs.nix` with [a deploy-rs node configuration](https://github.com/serokell/deploy-rs#node) that includes a system profile and which is also utilized by [a krops deployment](https://github.com/Mic92/krops)
*	`ssh-keys/default.nix` with configuration for `users.users.root.openssh.authorizedKeys.keys` utilized
	*	by [deploy-rs](https://github.com/serokell/deploy-rs) and [krops](https://github.com/Mic92/krops) to deploy via ssh to the node
	*	by [nixos-infect](https://github.com/elitak/nixos-infect) to set the ssh keys of an infected system (before the infection, the script copies all files from `ssh-keys` to `/etc/nixos/ssh-keys` of the infected node
*	`nixos-infect/default.nix` with configuration used by [nixos-infect](https://github.com/elitak/nixos-infect) and possibly utilizing resources in `nixos-infect/resources.nix`

## Checking the Configuration

~~~sh
nix flake check -L
~~~

## Building, Booting and/or Switching into the Local Node Configuration

~~~sh
nix run ".#build"
nix run ".#boot"
nix run ".#switch"
~~~

## Building And Analyzing a Node Configuration

~~~sh
nix run ".#build-nodeName"
nix run ".#size"
nix run ".#graph"
~~~

## Building a Virtual Maching of a Node

~~~sh
nix run ".#vm-nodeName"
~~~

## Infection of a non-NixOS node with a NixOS Basic System for this Node

Before the infection, it may be necessary to temporary modify `sshUser` system profile attribute in `deploy-rs.nix` to match the management user
(e.g., to "ubuntu" for OCI Ubuntu instances).

~~~sh
nix run ".#infect-nodeName"
~~~

## Deploy-RS: Local Build and Automatic Deployment of All Profiles of a Node

~~~sh
nix run '.#deploy-rs#nodeName'
~~~

For cross-compilation to build node with different architecture from a host node, set the host node option `boot.binfmt.emulatedSystems = [ "aarch64-linux" ]`.
That sets `boot.binfmt.registrations` and also `nix.settings.{extra-platforms,extra-sandbox-paths}` need for cross-build.

## Krops: Deployment of the Configuration and Remote Build and Deployment for a Node

~~~sh
nix run '.#deploy-krops#nodeName'
~~~

## SSH to a Node

~~~sh
nix run ".#ssh-nodeName"
~~~

## Nix Repl for the Node's Configuration

~~~sh
nix run ".#repl-nodeName"
~~~

## List System Profile Generations

~~~sh
# list all generations
nix-env --list-generations --profile /nix/var/nix/profiles/system

# print the last generation artifacts
ls -l /nix/var/nix/profiles/system-$(nix-env --list-generations --profile /nix/var/nix/profiles/system | sed 's/^\s*\([0-9]\+\)\s\+.*$/\1/g' | tail -1)-link/
~~~

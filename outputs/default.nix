myself:

inputs @ { self
, nixpkgs
, utils
, deploy-rs
, ...
}:

{
  # a directory of sub-directories with Nix files of system configurations for each managed system/node
  nodesDirPath ? (self.outPath + "/nodes")
  # an architecture of the local system
, localSystem ? (builtins.currentSystem or "x86_64-linux")
  # a list of architectures of supported systems
, supportedSystems ? myself.lib.defaultSystems
  # an attribute set of module configurations that should be loaded into system configurations of each system/node as its NixOS/HomeManager modules
  # (if the attr name is the same as an existing input name then the input name will be excluded from automatically loaded nixosModule/homeManagerModule attributes)
, loadedNixosModules ? { }
, loadedHomeManagerModules ? { }
}:

let

  nodePath = nodeName: pathSuffix:
    let
      path = nodesDirPath + "/${nodeName}/${pathSuffix}";
    in
    if (builtins.pathExists path) then path else null; # builtins.trace "Missing path '${path}'; the requested path value will be set to null!" null; # DISABLED: too noisy and there is tracing in nodeOptionallyApplyPathAsAttr

  nodeOptionallyApplyPathAsAttr = nodeName: pathSuffix: attrName: function:
    let
      path = nodePath nodeName pathSuffix;
    in
    if (path != null) then {
      ${attrName} = function path;
    } else builtins.trace "Path suffix '${pathSuffix}' does not exist for node '${nodeName}'; attribute '${attrName}' wont be set!" { };

  nodesNames =
    let
      dirEntryFileTypeFilter = _: fileType: (fileType == "directory");
    in
    builtins.attrNames (nixpkgs.lib.attrsets.filterAttrs dirEntryFileTypeFilter (builtins.readDir nodesDirPath));

  importNode = name:
    let
      path = nodePath name "";
    in
    {
      inherit name path;
      value = import (nodePath name "");
    }
    // nodeOptionallyApplyPathAsAttr name "deploy-rs.nix" "deployRs" import
    // nodeOptionallyApplyPathAsAttr name "system-arch.nix" "systemArch" (path: import path nixpkgs)
    // nodeOptionallyApplyPathAsAttr name "nixos-infect/resources.nix" "nixosInfectResources" import
    // nodeOptionallyApplyPathAsAttr name "ssh-keys/id_ed25519" "sshKeyFile" (path: path)
  ;

  nodes = builtins.map importNode nodesNames;

  genAttrsForNodes = nodes: filterOrBool: namePrefix: generator:
    let
      filteredNodes =
        if (builtins.isBool filterOrBool)
        then nixpkgs.lib.lists.optionals filterOrBool nodes
        else builtins.filter filterOrBool nodes;
      nodeToNameGeneratedValue = node: nixpkgs.lib.attrsets.nameValuePair (namePrefix + node.name) (generator node);
    in
    builtins.listToAttrs (builtins.map nodeToNameGeneratedValue filteredNodes);

in
(utils.lib.eachSystem supportedSystems (import ./each-system.nix myself inputs (genAttrsForNodes nodes))) // {

  nixosConfigurations = genAttrsForNodes nodes true "" (import ./each-configuration.nix myself inputs localSystem loadedNixosModules loadedHomeManagerModules);

  deploy.nodes =
    let

      mkDeployNode = node: nixpkgs.lib.attrsets.recursiveUpdate node.deployRs {
        profiles.system.path = deploy-rs.lib.${localSystem}.activate.nixos self.nixosConfigurations.${node.name};
      };

    in
    genAttrsForNodes nodes true "" mkDeployNode;

}

myself:

inputs @ { self
, nixpkgs
, nixpkgs-unstable
, home-manager
, ...
}:

localSystem:

loadedNixosModules:

loadedHomeManagerModules:

node:

let

  # Things in this set are passed to modules and accessible in the top-level arguments (e.g. `{ pkgs, lib, inputs, ... }:`).
  specialArgs = {
    inherit self inputs;
    hostName = node.name;
    # legacy argument to let know included modules that they are NixOS top-level modules and not HomeManager sub-modules
    moduleMode = "NixOS";
    # inherit unstable lib and pkgs
    lib-unstable = nixpkgs-unstable.lib;
    pkgs-unstable = import nixpkgs-unstable {
      inherit (node.systemArch) system;
      config = { allowUnfree = true; };
    };
  };

  hm-nixos-as-super = modules: args: { ... }: {
    # Submodules have merge semantics, making it possible to amend
    # the `home-manager.users` submodule for additional functionality.
    # see https://github.com/cideM/dotfiles/blob/master/flake.nix
    # and https://github.com/SoxinOS/soxin/blob/main/lib/nixos-system.nix
    options.home-manager.users = nixpkgs.lib.mkOption {
      type = nixpkgs.lib.types.attrsOf (nixpkgs.lib.types.submoduleWith {
        # Adds our own modules as home-manager sub-modules.
        inherit modules;
        specialArgs =
          # Makes args available to Home Manager modules.
          args // {
            # legacy argument to let know included modules that they are HomeManager sub-modules and not NixOS top-level modules
            moduleMode = "home-manager";
          };
      });
    };
  };

  setup-module = {
    # let 'nixos-version --json' know about the Git revision of this flake.
    system.configurationRevision = nixpkgs.lib.mkIf (self ? rev) self.rev;
    # use experimental Nix Flakes repositories, https://discourse.nixos.org/t/using-experimental-nix-features-in-nixos-and-when-they-will-land-in-stable/7401/4
    # see also https://www.tweag.io/blog/2020-05-25-flakes/
    nix.package = nixpkgs.lib.mkForce nixpkgs.legacyPackages.${node.systemArch.system}.nixFlakes;
    nix.extraOptions = ''
      # Nix Flakes
      experimental-features = nix-command flakes
    '';
    nix.nixPath = (nixpkgs.lib.attrsets.mapAttrsToList (name: value: "${name}=${value}") inputs) ++ [ "nixos-config=${node.path}" ];
    nix.registry = nixpkgs.lib.attrsets.mapAttrs (_: flake: { inherit flake; }) inputs;
    # DO NOT set nixpkgs.{crossSystem,localSystem} as the cross-building generally does not work (e.g., "Invalid ELF image for this architecture" errors)
    # USE system attr to set a target-system arch and then build on a host with boot.binfmt.emulatedSystems set to the target-system arch which is slower (uses emulation) but stable; see https://nixos.wiki/wiki/NixOS_on_ARM#Compiling_through_binfmt_QEMU
  };

  attrsOfInputsExceptFor = attrPath: excludedInputNames:
    let
      inputsWithAttrName = nixpkgs.lib.attrsets.filterAttrs (inputName: input: (nixpkgs.lib.attrsets.hasAttrByPath attrPath input) && ! builtins.elem inputName excludedInputNames) inputs;
    in
    nixpkgs.lib.attrsets.mapAttrsToList (inputName: input: builtins.trace "${node.name}: Using '${builtins.concatStringsSep "." attrPath}' from '${inputName}'." (nixpkgs.lib.attrsets.getAttrFromPath attrPath input)) inputsWithAttrName;

  nixosModuleAttrsOfInputsExceptFor = excludedInputNames: (attrsOfInputsExceptFor [ "nixosModule" ] excludedInputNames) ++ (attrsOfInputsExceptFor [ "nixosModules" "default" ] excludedInputNames);
  homeManagerModuleAttrsOfInputsExceptFor = excludedInputNames: (attrsOfInputsExceptFor [ "homeManagerModule" ] excludedInputNames) ++ (attrsOfInputsExceptFor [ "homeManagerModules" "default" ] excludedInputNames);

in
nixpkgs.lib.nixosSystem {

  inherit (node.systemArch) system;
  inherit specialArgs;

  modules =
    let
      valuesOfLoadedNixosModules = builtins.attrValues loadedNixosModules;
      valuesOfLoadedHomeManagerModules = builtins.attrValues loadedHomeManagerModules;
      nixosModuleAttrsOfInputs = nixosModuleAttrsOfInputsExceptFor ([ "nixpkgs-unstable" "home-manager" ] ++ (builtins.attrNames loadedNixosModules));
      homeManagerModuleAttrsOfInputs = homeManagerModuleAttrsOfInputsExceptFor ([ "nixpkgs-unstable" ] ++ (builtins.attrNames loadedHomeManagerModules));
    in
    valuesOfLoadedNixosModules ++ nixosModuleAttrsOfInputs ++ [

      setup-module

      nixpkgs.nixosModules.notDetected

      home-manager.nixosModules.home-manager
      {
        ## the wollowing settings is required for building by flake; see https://github.com/nix-community/home-manager#nix-flakes
        # use the global pkgs that is configured via the system level nixpkgs options
        # which saves an extra Nixpkgs evaluation, adds consistency, and removes the dependency on NIX_PATH,
        # which is otherwise used for importing Nixpkgs
        home-manager.useUserPackages = true;
        # packages will be installed to /etc/profiles instead of $HOME/.nix-profile
        home-manager.useGlobalPkgs = true;
        # set Home Manager session variables in login shell (HM does it automatically only if it manages your shell configuration)
        # see https://nix-community.github.io/home-manager/index.html#_why_are_the_session_variables_not_set
        # using extraInit instead of loginShellInit to override system-wide vars such as EDITOR; see https://github.com/nix-community/home-manager/issues/1011#issuecomment-606869802
        environment.extraInit = ''
          ## --BEGIN-- added by flake-base/nixos
          # source HM session variables; see https://github.com/nix-community/home-manager/blob/master/docs/faq.adoc#why-are-the-session-variables-not-set
          # this works both in POSIX compatible shells (bash, Z) and in fish with programs.fish.useBabelfish enabled (which is better than using fenv for fish)
          # for home-manager.useGlobalPkgs == true
          test -r "/etc/profiles/per-user/$USER/etc/profile.d/hm-session-vars.sh" && source "/etc/profiles/per-user/$USER/etc/profile.d/hm-session-vars.sh" >/dev/null 2>&1 || true
          # for home-manager.useGlobalPkgs == false
          test -r "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh" && source "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh" >/dev/null 2>&1 || true
          ## -- END --
        '';
      }
      (hm-nixos-as-super (valuesOfLoadedHomeManagerModules ++ homeManagerModuleAttrsOfInputs) specialArgs)

      node.value
    ];

}
